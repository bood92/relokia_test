`<?php

class Tasks
{

    /**
     * 1. Перевернути стрічку, не використовуючи функції strrev().
     * $string = ‘string’;
     */
    public function task1()
    {
        $string = 'string';

        $result = '';
        for ($i = (strlen($string) - 1); $i >= 0; $i--) {
            $result .= $string[$i];
        }

        return $result;
    }

    /**
     * 2. Чи буде
     * $a == $b? - Так
     * $b == $c? - Ні
     * $a == $c? - Так
     *
     * $a = 0;
     * $b = null;
     * $c = “0”;
     *
     */

    /**
     * 3. Регулярний вираз.
     * Написати регулярний вираз, який буде валідувати email адрес.
     *
     * Результат:
     * test.mail@mail.com // true
     * user@site.ua //true
     * -user@site.ua //false
     * user-email@site //false
     */
    public function task3($mail = '-user@site.ua')
    {
        return (bool)preg_match('/^\w+[.]{0,1}\w+[@]{1}[a-z]+[.]{0,1}[a-z]+$/', $mail);
    }

    /**
     *
     * 4. Перевернути масив.
     * Є масив $array = ['h', 'e', 'l', 'l', 'o'], як з нього отримати ['o', 'l', 'l', 'e', ' h '], не використовуючи функції array_reverse.
     */
    public function task4()
    {
        $array = ['h', 'e', 'l', 'l', 'o'];

        $result = [];
        for ($i = (count($array) - 1); $i >= 0; $i--) {
            $result[] = $array[$i];
        }

        return $result;
    }

    /**
     * 5.Обробка масиву.
     * Дано масив:
     * $array = [1, 2, 3, 4, 5];
     *
     * Видалити елемент із наведеного масиву. Після видалення елемента цілочисельні ключі масиву повинні бути впорядкованими.
     */
    public function task5()
    {
        $array = [1, 2, 3, 4, 5];
        unset($array[1]);
        return array_values($array);
    }

    /**
     * 6. Запис даних в масив.
     * Дано масив:
     * $array = [1, 2, 3, 4, -5, 6, 7, -8, 9, -10];
     *
     * Після кожного негативного елемента масиву, вставити елемент з нульовим значенням.
     */
    public function task6()
    {
        $array = [1, 2, 3, 4, -5, 6, 7, -8, 9, -10];
        $result = [];
        foreach ($array as $key => $item) {
            $result[] = $item;
            if ($item < 0) {
                $result[] = 0;
            }
        }
        return $result;
    }

    /**
     * 7. Сортування массиву.
     * Дано масив:
     * $array = [0 => 7, 1 => 1, 2 => 4, 3 => 2, 4 => 5];
     *
     * Потрібно відсортувати масив, не використовуючи php функцій для сортування масивів.
     */
    public function task7()
    {
        $array = [0 => 7, 1 => 1, 2 => 4, 3 => 2, 4 => 5];

        for ($i = 0; $i < count($array); $i++) {
            for ($j = $i + 1; $j < count($array); $j++) {
                if ($array[$i] > $array[$j]) {
                    $temp = $array[$j];
                    $array[$j] = $array[$i];
                    $array[$i] = $temp;
                }
            }
        }

        return $array;
    }

    /**
     * 8. Записати дані з масиву в csv файл.
     * Дано два массива
     * Результатом повинен бути CSV файл, де заголовками є ключі масиву, а даними, значення масивів.
     * Значення, в яких є однакові ключі, змерджити в одне.
     *
     * ----
     * !!! Нічого кращого не придумав
     */
    public function task8()
    {
        $one = [
            'title' => 'Test title one',
            'description' => 'Test description one',
            'author' => 'Igor Kril'
        ];

        $two = [
            'description' => 'Test description two',
            'date' => '2019-01-01',
            'title' => 'Test title two',
            'phone' => '0981111111'
        ];

        foreach (array_diff_key($one, $two) as $key => $item) {
            $two[$key] = '';
        }

        foreach (array_diff_key($two, $one) as $key => $item) {
            $one[$key] = '';
        }

        $title = array_keys($one);
        sort($title, SORT_ASC);
        $title = "'" . implode($title, "','") . "'";

        $rows = [];
        foreach ([$one, $two] as $item) {
            ksort($item, SORT_ASC);
            $rows[] = "'" . implode(array_values($item), "','") . "'";
        }

        $write = array_merge([$title], $rows);
        $fp = fopen('task8.csv', 'w');
        fputcsv($fp, $write, ';');
        fclose($fp);
    }


    /**
     * 9. Функція
     * Написати функцію, яка генерує 3 випадкових числа в діапазоні від 0 до 10. Якщо сума цих чисел менше 14, згенерувати нову трійку.
     */
    public function task9()
    {
        $sum = 0;
        while ($sum < 14) {
            $sum = array_sum([rand(0, 10), rand(0, 10), rand(0, 10)]);
        }

        return $sum;
    }

    /**
     * 10. Рекурсія
     * Написати рекурсивну функцію, яка обчислює факторіал числа.
     */
    public function task10($n)
    {
        if ($n == 0) return 1;
        return $n * $this->task10($n - 1);
    }

    /**
     * @return mixed
     * 11. Отримати IP-адресу
     * Як отримати IP-адресу клієнта в PHP.
     */
    public function task11()
    {
        return $_SERVER['REMOTE_ADDR'];
    }
}