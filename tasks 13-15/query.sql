/*
13. Виконати вибірку (MySQL).
*/
SELECT user.user_name , migration.migration_name
FROM relations
INNER JOIN `user` ON relations.user_id = `user`.user_id
INNER JOIN migration ON relations.migration_id = migration.migration_id;

/**
14. Виконати вибірку (MySQL).
Є дві таблиці:
user - таблиця з користувачами (users_id, name)
order - таблиця із замовленнями (orders_id, users_id, status)
**/
SELECT `user`.*
FROM `user`
WHERE `user_id` IN
(
	SELECT user_id
	FROM `order`
	WHERE `status`=1
	GROUP BY `user_id`
	HAVING COUNT(*) > 5
)

/*
15. Вивести унікальні значення (MySQL).
Написати запит, який виведе тільки унікальні значення із стовпця name
*/
SELECT *
FROM `user`
GROUP BY user_name
HAVING COUNT(user_name) = 1