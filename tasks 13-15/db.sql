-- --------------------------------------------------------
-- Сервер:                       127.0.0.1
-- Версія сервера:               10.3.22-MariaDB - mariadb.org binary distribution
-- ОС сервера:                   Win64
-- HeidiSQL Версія:              11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for relokia_test
CREATE DATABASE IF NOT EXISTS `relokia_test` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `relokia_test`;

-- Dumping structure for таблиця relokia_test.migration
CREATE TABLE IF NOT EXISTS `migration` (
  `migration_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `migration_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`migration_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table relokia_test.migration: ~6 rows (приблизно)
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` (`migration_id`, `migration_name`) VALUES
	(1, '10'),
	(2, '11'),
	(3, '12'),
	(4, '10'),
	(5, '11'),
	(6, '12');
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;

-- Dumping structure for таблиця relokia_test.order
CREATE TABLE IF NOT EXISTS `order` (
  `orders_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`orders_id`),
  KEY `order_user_id_foreign` (`user_id`),
  CONSTRAINT `order_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table relokia_test.order: ~20 rows (приблизно)
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` (`orders_id`, `user_id`, `status`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 1, 1),
	(4, 1, 1),
	(5, 1, 1),
	(6, 2, 1),
	(7, 2, 1),
	(8, 3, 1),
	(9, 1, 1),
	(10, 1, 1),
	(11, 1, 1),
	(12, 1, 2),
	(13, 1, 1),
	(14, 1, 1),
	(15, 1, 1),
	(16, 2, 1),
	(17, 2, 1),
	(18, 3, 1),
	(19, 1, 1),
	(20, 1, 1);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Dumping structure for таблиця relokia_test.relations
CREATE TABLE IF NOT EXISTS `relations` (
  `user_id` bigint(20) unsigned NOT NULL,
  `migration_id` bigint(20) unsigned NOT NULL,
  KEY `relations_user_id_foreign` (`user_id`),
  KEY `relations_migration_id_foreign` (`migration_id`),
  CONSTRAINT `relations_migration_id_foreign` FOREIGN KEY (`migration_id`) REFERENCES `migration` (`migration_id`),
  CONSTRAINT `relations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table relokia_test.relations: ~12 rows (приблизно)
/*!40000 ALTER TABLE `relations` DISABLE KEYS */;
INSERT INTO `relations` (`user_id`, `migration_id`) VALUES
	(1, 3),
	(2, 2),
	(3, 1),
	(3, 3),
	(3, 2),
	(1, 2),
	(1, 3),
	(2, 2),
	(3, 1),
	(3, 3),
	(3, 2),
	(1, 2);
/*!40000 ALTER TABLE `relations` ENABLE KEYS */;

-- Dumping structure for таблиця relokia_test.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table relokia_test.user: ~10 rows (приблизно)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `user_name`) VALUES
	(1, 'Igor'),
	(2, 'Ira'),
	(3, 'Max'),
	(4, 'Igor'),
	(5, 'Max'),
	(6, 'Igor'),
	(7, 'Ira'),
	(8, 'Max'),
	(9, 'Igor'),
	(10, 'Max');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
